import os
import time
import traceback
from tempfile import mkstemp
import requests
import http.cookiejar
from bs4 import BeautifulSoup
from transmission_rpc import Client
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-ku', '--kg-user', help='Karagarga username', default=os.getenv('KARAGARGA_USER', 'user'))
parser.add_argument('-kp', '--kg-pass', help='Karagarga password', default=os.getenv('KARAGARGA_PASS', 'pass'))
parser.add_argument('-tu', '--tm-user', help='Transmission username', default=os.getenv('TRANSMISSION_USER', None))
parser.add_argument('-tp', '--tm-pass', help='Transmission password', default=os.getenv('TRANSMISSION_PASS', None))
parser.add_argument('-th', '--tm-host', help='Transmission host', default=os.getenv('TRANSMISSION_HOST', 'localhost'))
parser.add_argument('-tP', '--tm-port', type=int, help='Transmission port', default=os.getenv('TRANSMISSION_PORT', 9091))
parser.add_argument('-tpr', '--tm-proto', help='Transmission protocol', default=os.getenv('TRANSMISSION_PROTO', 'http'))
args = parser.parse_args()

kg_host = 'https://karagarga.in'
active_torrents = []
session = requests.Session()
session.cookies.set('uid', args.kg_user, domain='.karagarga.in')
session.cookies.set('pass', args.kg_pass, domain='.karagarga.in')
transclient = Client(host=args.tm_host, port=args.tm_port, protocol=args.tm_proto,
                     username=args.tm_user, password=args.tm_pass)


# Get list of currently active torrents, by it's URL
def get_active_torrents():
    r = session.get(f'{kg_host}/current.php?id={args.kg_user}')
    soup = BeautifulSoup(r.text, 'html.parser')
    tables = soup.findAll('table', {'id': 'browse'})
    for table in tables:
        seeds = table.findAll('tr')[1:]
        for seed in seeds:
            if seed.findAll('iframe'):
                continue
            active_torrents.append(seed.find('span').a.get('href'))


# Download all torrents in the list and upload them to Transmission via RPC afterwards
def add_torrent_to_transmission(download_url, **kwargs):
    r = session.get(f'{kg_host}/{download_url}')
    fd, path = mkstemp()
    with open(path, 'wb') as f:
        f.write(r.content)
    with open(path, 'rb') as f:
        transclient.add_torrent(f, **kwargs)
    os.close(fd)


def scrape_kg(category, priority):
    r = session.get(f'{kg_host}/browse.php?{category}=1')
    soup = BeautifulSoup(r.text, 'html.parser')
    # Get list of all newest titles
    titles = soup.find('table', {'id': 'browse'}).findAll('tr')[1:]
    for title in titles:
        # Get rid of weird iframe tr's between actual elements in the table (is that some kind of obfuscation?)
        if title.findAll('iframe'):
            continue
        # Skip already downloaded titles
        if 'snatchedrow' in title.get('class'):
            continue
        title_name = title.find('span').get_text()
        title_url = title.find('span').a.get('href')
        # Unless the torrent is an active torrent (which doesn't necessarily yet has 'snatched' status),
        # get a download link and append it to queue
        if title_url not in active_torrents:
            r = session.get(f'{kg_host}/{title_url}')
            soup = BeautifulSoup(r.text, 'html.parser')
            try:
                torrent_url = [el.get('href') for el in soup.findAll('a', {'class': 'index'})][0]
                add_torrent_to_transmission(torrent_url, bandwidthPriority=priority)
                print(f'Downloading title:"{title_name}", URL:{kg_host}/{title_url}, priority:{priority} ..')
            except:
                print(f'Error parsing download URL from title page {title_url}!')


try:
    get_active_torrents()
    # Scrape 'freeleech' category with Transmission bandwidthPriority=1 (high)
    scrape_kg('fl', 1)
    # Scrape 'featured' category with Transmission bandwidthPriority=0 (medium)
    scrape_kg('ft', 0)
except Exception as err:
    traceback.print_exc()
    exit(1)
