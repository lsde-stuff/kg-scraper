FROM python:alpine
USER root
RUN adduser -u 1000 -D -h /tmp user
ENV TZ=Europe/Prague
USER user
WORKDIR /tmp
COPY kg-scraper.py requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
ENTRYPOINT ["python", "-u", "kg-scraper.py"]
